import React from 'react';
import '../../styles/contacto.css';

const Contacto = () => {
    return (
        <form>
            <legend> Formulario de contacto </legend>
            <div className='input-field'>
                <label> Nombre: </label>
                <input type='text' placeholder='Introduce tu nombre...' />
            </div>
            <div className='input-field'>
                <label> Email: </label>
                <input type='text' placeholder='Introduce tu email...' />
            </div>
            <div className='input-field'>
                <label> Mensaje: </label>
                <textarea>
                </textarea>
            </div>
            <div className='input-field enviar'>
                <input type='submit' value='Enviar' />
            </div>
        </form>
    );
}

export default Contacto;