import React from 'react';
import '../../styles/producto.css';

const DetalleProducto = ( props ) => {
    if( !props.producto ) return <h2> Producto no encontrado </h2>
    const {id, nombre, precio, imagen, descripcion} = props.producto;
    return (
            <div className='info-producto'>
                <div className='imagen'>
                    <img src={'/img/' + imagen + '.png'} alt={nombre} title={nombre}/>
                </div>
                <div className='info'>
                    <h2> {nombre} </h2>
                    <p className='precio'> ${precio} </p>
                    <p> {descripcion} </p>
                </div>
            </div>
    );
}

export default DetalleProducto;